## Descrição do Projeto

Este projeto é o back-end de uma calculadora de tinta desenvolvida em Node.js, utilizando as bibliotecas Express, CORS e Body-Parser. O objetivo deste repositório é fornecer toda a estrutura necessária para receber dados do front-end e realizar os cálculos para determinar a quantidade de tinta necessária para pintar uma sala. O back-end recebe informações como altura e largura das paredes, bem como a quantidade de portas e janelas que o usuário informar através do front-end.

Esse projeto esta rodando na porta 5500 local
http://localhost:5500

## Dependências

Para iniciar o projeto, é necessário instalar algumas dependências. No console, execute um dos seguintes comandos:
```bash

npm install

ou

yarn install

```
## Executando o Projeto

Após instalar as dependências, para iniciar o servidor, execute o seguinte comando no console:

node index.js

## Estrutura do Projeto

O servidor foi configurado para receber requisições POST na rota /calculate. Os dados enviados pelo front-end incluem a altura e a largura de cada uma das quatro paredes, além do número de portas e janelas em cada parede. O servidor então processa esses dados para calcular a área total a ser pintada, descontando as áreas ocupadas por portas e janelas. Com base na área total, o servidor calcula a quantidade de tinta necessária e sugere a quantidade e tamanhos das latas de tinta a serem compradas, priorizando sempre as latas maiores para otimizar a compra.

## Exemplo de Requisição

Abaixo está um exemplo em JSON de como os dados devem ser enviados do front-end para o back-end:
```json
{
  "walls": [
    { "height": 3.0, "width": 4.0 },
    { "height": 3.0, "width": 4.0 },
    { "height": 3.0, "width": 4.0 },
    { "height": 3.0, "width": 4.0 }
  ],
  "doors": [1, 1, 0, 0],
  "windows": [2, 0, 1, 1]
}
```
## Lógica de Cálculo

- Área das Paredes: O servidor calcula a área total de cada parede multiplicando a altura pela largura.

- Desconto de Portas e Janelas: Subtrai a área ocupada por portas (2m x 0.8m) e janelas (1.2m x 1.2m) da área total das paredes.

- Cálculo de Tinta: Com a área total a ser pintada, divide-se pelo valor de cobertura da tinta (1 litro cobre 5m²).

- Sugestão de Latas: O servidor sugere a quantidade de latas necessárias, priorizando sempre as latas maiores para otimizar a compra.

- Este projeto é uma solução prática para calcular de forma eficiente a quantidade de tinta necessária, ajudando os usuários a economizar tempo e recursos.

## Regras de Negócio

- Dimensões da Parede: Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.
- Área de Portas e Janelas: O total de área das portas e janelas deve ser no máximo 50% da área da parede.
- Altura da Parede com Porta: A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.
- Dimensões das Portas e Janelas:
- Cada porta possui as medidas: 0,80 x 1,90 metros.
- Cada janela possui as medidas: 2,00 x 1,20 metros.
- Cobertura da Tinta: Cada litro de tinta é capaz de pintar 5 metros quadrados.
- Variações de Tamanho das Latas de Tinta:

0,5 L 

2,5 L

3,6 L

18 L